<?php ?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Chat App</title>
   <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
   <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
</head>
<body>
    <div class="chat-wrapper">
        <div class="chat-title">Chat-LUNATIKI</div>

        <div class="chat-body chat-container">
<!--            <div class="message message__user">-->
<!--                <div class="message-text message-text__user">-->
<!--                    <span class="username">User Name</span>-->
<!--                    <p>Some questionSome answer from manager Some answer from manager Some answer from manager Some answer from managerSome answer from manager Some answer from manager Some answer from manager Some answer from manager from user</p>-->
<!--                    <span class="time">6 minutes ago</span>-->
<!--                </div>-->
<!--                <div class="user-pic"></div>-->
<!--            </div>-->
<!--            <div class="message message__manager">-->
<!--                <div class="manager-pic"></div>-->
<!--                <div class="message-text message-text__manager">-->
<!--                    <span class="username">Manager Name</span>-->
<!--                    <p>Some answer from manager Some answer from manager Some answer from manager Some answer from manager</p>-->
<!--                    <span class="time">5 minutes ago</span>-->
<!--                </div>-->
<!--            </div>-->
        </div>

        <div class="inputs-container">
            <form class="chat-input">
                <input type="text" id="user_message" class="type-msg" placeholder="Type message...">
                <input type="button"  id="send_button" class="send-btn" value="Send">
            </form>
        </div>

    </div>

    <script src="index.js"></script>
    <script src="front.js"></script>
    <script>
        $('.inputs-container').keydown(function(e) {
            if (!e.shiftKey && e.keyCode === 13) {
                $('#send_button').click();
                e.preventDefault();
            }
        });
    </script>
</body>
</html>