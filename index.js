let button = document.getElementById("send_button");
let messageField = document.getElementById('user_message');
let chatID = 0;

button.addEventListener("click", function () {
    let xhr = new XMLHttpRequest();
    let formData = new FormData();
    formData.append("user_message", messageField.value.trim().substring(0, 3500));
    messageField.value = "";

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            let response = JSON.parse(xhr.response);
            showMessage(response, chatID);
        }
    };

    xhr.open("POST", "./send.php", true);
    xhr.send(formData);
    update();

});

function update() {
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            let response = JSON.parse(xhr.response);
            response.forEach(obj => {
                showMessage(obj.message);
            });
        }
    };

    xhr.open("GET", "./update.php", true);
    xhr.send();
    setTimeout(update, 5000);
}

