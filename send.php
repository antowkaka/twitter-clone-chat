<?php
session_start();
$message = htmlspecialchars($_POST['user_message']);
$client_id = $_SESSION['chat_id'];

$message_with_id = urlencode("<b><i>chat $client_id:</i></b>\n\n".$message);

$send = "https://api.telegram.org/bot{$_SESSION["token"]}/sendMessage?chat_id={$_SESSION["chat_tg"]}&parse_mode=html&text={$message_with_id}";

$ch = curl_init($send);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);
$response_parsed = json_decode($response, true)['result'];

array_push($_SESSION["history"], $response_parsed);


if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
    echo json_encode(["from" => ["first_name" => $response_parsed['from']['first_name'], "is_bot" => true], "date" => $response_parsed['date'], "text" => $message]);
} else {
    echo json_encode(["from" => ["first_name" => "Your PHP server", "is_bot" => false], "date" => 0, "text" => "An error has occurred\n$response"]);
}
curl_close($ch);
