let chatContainer = document.querySelector('.chat-container');

function showMessage(message) {
    let messageText = message.text;
    let time = new Date(message.date * 1000);
    let chatMessage = document.createElement('div');
    chatMessage.classList.add('message', 'message__user');
    let userName = message.from.is_bot ? 'You' : message.from.first_name;
    if (userName === 'You') {
        chatMessage.innerHTML = `<div class="message-text message-text__user">
                                 <span class="user-name username">${userName}</span><p>${messageText}</p>
                                 <span class="time">${(time.getHours() < 10 ? ('0' + time.getHours()) : time.getHours())}:${(time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes())}</span>
                                 </div>
                                 <div class="user-pic"></div>`;
    } else {
        chatMessage.classList.remove('message__user');
        chatMessage.classList.add('message__manager');
        chatMessage.innerHTML = `<div class="manager-pic"></div>
                                 <div class="message-text message-text__manager">
                                 <span class="user-name username">${userName}</span><p>${messageText}</p>
                                 <span class="time">${(time.getHours() < 10 ? ('0' + time.getHours()) : time.getHours())}:${(time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes())}</span>
                                 </div>`;
    }
    chatContainer.append(chatMessage);
    scroll();

    button.setAttribute('disabled','disabled');
}

function scroll() {
    chatContainer.scrollTop = 9999;
}

messageField.addEventListener('input', () => {
   if(messageField.value.trim().length === 0) {
       button.setAttribute('disabled','disabled');
   }
   else {
       button.removeAttribute('disabled');
   }
});