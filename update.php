<?php

session_start();
$offset = $_SESSION["update_offset"];
$getUpdates = "https://api.telegram.org/bot{$_SESSION["token"]}/getUpdates?allowed_updates=['message']&offset=-40";

$ch = curl_init($getUpdates);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);

$results = [];


if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {

    foreach (json_decode($response, true)['result'] as &$update){
        if(array_key_exists("reply_to_message", $update['message'])){
            if($update['update_id'] > $_SESSION["update_offset"]) {
                foreach ($_SESSION["history"] as &$obj){
                    if($obj['message_id'] === $update['message']['reply_to_message']['message_id']){
                        $update['message']['text'] = htmlspecialchars($update['message']['text'], ENT_HTML5);
                        array_push($results, $update);
                        $_SESSION["update_offset"] = $update['update_id'];
                    }
                }
            }
        }
    }

    echo json_encode($results);
} else {
    echo json_encode(["from" => ["first_name" => "Your PHP server", "is_bot" => false], "date" => 0, "text" => "An error has occurred\n$response"]);
}
curl_close($ch);

